package br.com.senac.ola;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText  campoNome ;
    private Button  btnOk ;
    private TextView textViewMensagem ;
    private TextView labelNome ;



    private void initComponentes(){
        this.campoNome = (EditText) findViewById(R.id.txtNome);
        this.btnOk =  (Button) findViewById(R.id.btnOk);
        this.textViewMensagem = (TextView) findViewById(R.id.mensagem) ;
        this.labelNome = (TextView) findViewById(R.id.labelNome) ;
    }

    private String getText(EditText e){
        String texto =  e.getText().toString() ;
        e.setText("");
        e.onEditorAction(EditorInfo.IME_ACTION_DONE);

        return  texto ;
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponentes() ;




        this.btnOk.setOnClickListener
        (

         new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nome =  getText(campoNome) ;
                labelNome.setText(nome);
                textViewMensagem.setVisibility(View.VISIBLE);
                labelNome.setVisibility(View.VISIBLE);
                Log.i("#OLA" , "Nome:" + nome) ;
            }
        }

        );








    }


}
